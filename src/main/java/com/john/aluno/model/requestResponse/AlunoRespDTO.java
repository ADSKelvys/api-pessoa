package com.john.aluno.model.requestResponse;

public class AlunoRespDTO {
    private Long id;
    private String name;
    private String cpf;
    private String cidadeEndereco;
    private String ufEndereco;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCpf() {
        return cpf;
    }
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    public String getCidadeEndereco() {
        return cidadeEndereco;
    }
    public void setCidadeEndereco(String cidadeEndereco) {
        this.cidadeEndereco = cidadeEndereco;
    }
    public String getUfEndereco() {
        return ufEndereco;
    }
    public void setUfEndereco(String ufEndereco) {
        this.ufEndereco = ufEndereco;
    }

}
