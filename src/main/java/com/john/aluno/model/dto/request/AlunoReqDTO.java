package com.john.aluno.model.dto.request;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.br.CPF;

import com.john.aluno.model.Endereco;

public class AlunoReqDTO {

    @NotBlank(message = "Campo requirido!!!")
    private String name;

    @CPF(message = "CPF invalido!!!")
    private String cpf;

    private Endereco endereco;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
}
