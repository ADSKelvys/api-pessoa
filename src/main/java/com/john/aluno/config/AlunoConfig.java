package com.john.aluno.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AlunoConfig {
    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }
}
