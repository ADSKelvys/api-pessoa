package com.john.aluno.repository;


import com.john.aluno.model.Aluno;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlunoRepository extends JpaRepository<Aluno, Long> {

    List<Aluno> findAllByEnderecoUfIgnoreCase(String uf);

    Optional<Aluno> findByCpfAndEnderecoUfIgnoreCase(String cpf, String uf);

    Optional<Aluno> findByCpf(String cpf);


}