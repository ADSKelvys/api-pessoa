package com.john.aluno.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.john.aluno.model.dto.request.AlunoReqDTO;
import com.john.aluno.model.requestResponse.AlunoRespDTO;
import com.john.aluno.service.AlunoService;

@RestController
@RequestMapping("/alunos")
public class AlunoController {

    @Autowired
    private AlunoService alunoService;

    @GetMapping
    public ResponseEntity<List<AlunoRespDTO>> listAluno() {
        return ResponseEntity.ok(alunoService.listar());
    }

    @GetMapping("/{uf}")
    public ResponseEntity<List<AlunoRespDTO>> listByUf(@PathVariable ("uf") String uf)
    throws Exception{
    return ResponseEntity.ok(alunoService.findAllUf(uf));
    }
    @GetMapping("/{cpf}/{uf}")
    public ResponseEntity<AlunoRespDTO> listByUf(@PathVariable("cpf") String cpf, @PathVariable("uf") String uf)
            throws Exception {
        return ResponseEntity.ok(alunoService.findByAlunoUf(cpf, uf));
    }

    @GetMapping("/test")
    public ResponseEntity<AlunoRespDTO> list(@RequestParam(value = "cpf", required = true) String cpf,
            @RequestParam(value = "uf", required = false) String uf)
            throws Exception {
        return ResponseEntity.ok(alunoService.findByAlunoUf(cpf, uf));
    }

    @PostMapping
    public ResponseEntity<AlunoRespDTO> createAluno(@Valid @RequestBody AlunoReqDTO alunoReqDTO) {
        return ResponseEntity.ok(alunoService.createAluno(alunoReqDTO));
    }
}
