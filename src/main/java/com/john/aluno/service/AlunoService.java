package com.john.aluno.service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.john.aluno.model.Aluno;
import com.john.aluno.model.dto.request.AlunoReqDTO;
import com.john.aluno.model.requestResponse.AlunoRespDTO;
import com.john.aluno.repository.AlunoRepository;

@Service
public class AlunoService {

    @Autowired
    private AlunoRepository alunoRepository;

    @Autowired
    private ModelMapper modelMapper;

    public AlunoRespDTO createAluno(@Valid AlunoReqDTO alunoReqDTO) {
        Aluno aluno = new Aluno(
                alunoReqDTO.getName(),
                alunoReqDTO.getCpf(),
                alunoReqDTO.getEndereco());

        aluno = alunoRepository.save(aluno);
        return toAlunoRespDTO(aluno);
    }

    public List<AlunoRespDTO> listar() {
        List<Aluno> lista = alunoRepository.findAll();

        return lista.stream()
                .map(al -> toAlunoRespDTO(al))
                .collect(Collectors.toList());
    }

    public AlunoRespDTO findByAlunoUf(String cpf, String uf) throws Exception {

        if (Objects.isNull(cpf) && (Objects.isNull(uf))) {
            new RuntimeException("CPF e UF não pode ser nullo!");
        }
        if (Objects.isNull(uf)) {
            var aluno = alunoRepository.findByCpf(cpf).orElseThrow(() -> new Exception("Aluno não encontrado!"));
            return toAlunoRespDTO(aluno);
        }
        var al = alunoRepository.findByCpfAndEnderecoUfIgnoreCase(cpf, uf)
                .orElseThrow(() -> new Exception("Aluno não encontrado!"));

        return toAlunoRespDTO(al);
    }

    public List<AlunoRespDTO> findAllUf(String uf) {
        var alunoList = alunoRepository.findAllByEnderecoUfIgnoreCase(uf);
        return alunoList
                .stream()
                .map(al -> toAlunoRespDTO(al))
                .collect(Collectors.toList());
    }

    private AlunoRespDTO toAlunoRespDTO(Aluno aluno) {

        // AlunoRespDTO response = new AlunoRespDTO();
        // response.setName(aluno.getName());
        // response.setCpf(aluno.getCpf());
        // return response;

        return modelMapper.map(aluno, AlunoRespDTO.class);
    }
}
